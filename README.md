# Geopagos - Ejercicio API 

Desarrollar una API Rest en JAVA, para el registro y obtención de datos de figuras geométricas (cuadrado, triángulo, círculo) y sus distintos parámetros (superficie; base; altura; diámetro; tipo de figura geométrica).

### Entorno 📋

_IDE: Intellij Idea 2019_

_Base de Datos: MySQL_

_Framework: Spring boot_

_Lombok: Evita la implementacion de getters y setters_

## Ejecutando las pruebas ⚙

_URL's_
```
http://localhost:8080/figura/create
```
```
http://localhost:8080/figura/by?tipo=triangulo
```
```
http://localhost:8080/figura/by/3
```
```
http://localhost:8080/figura/all
```