package com.geopagos.geopagosapi.IServices;

import com.geopagos.geopagosapi.Entities.Figura;

import java.util.List;
import java.util.Optional;

public interface IFigura {
    Figura createFigura(Figura figura);
    List<Figura> getAllFigurasList();
    Optional<Figura> getFiguraById(Long id);
    List<Figura> getFiguraByTipo(String tipo);
}
