package com.geopagos.geopagosapi.Controllers;

import com.geopagos.geopagosapi.Entities.Figura;
import com.geopagos.geopagosapi.IServices.IFigura;
import com.geopagos.geopagosapi.helper.Calculo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/figura")
public class FiguraController {

    @Autowired
    private IFigura iFigura;

    Calculo calculo = new Calculo();

    @PostMapping("/create")
    public Figura create(@RequestBody Figura figura){
        figura = calculo.calculoSuperficie(figura);
        return iFigura.createFigura(figura);
    }

    @GetMapping("/all")
    public List<Figura> getAll(){
        return iFigura.getAllFigurasList();
    }

    @GetMapping("/by/{id}")
    public Optional<Figura> findById(@PathVariable(name= "id",required= true) Long id){
        return iFigura.getFiguraById(id);
    }

    @GetMapping("/by")
    public List<Figura> findByTipo(@RequestParam(name= "tipo",required= true) String tipo){
        return iFigura.getFiguraByTipo(tipo);
    }

}
