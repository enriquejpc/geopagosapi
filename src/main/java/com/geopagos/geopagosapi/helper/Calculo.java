package com.geopagos.geopagosapi.helper;

import com.geopagos.geopagosapi.Entities.Figura;

public class Calculo {

    public Figura calculoSuperficie(Figura figura)  {
        figura.setTipo(figura.getTipo().toLowerCase());
        Double superficie=0d;
        switch (figura.getTipo().toLowerCase()){
            case "circulo":
                superficie = ((Math.PI)* Math.pow((figura.getDiametro()/2),2));
                break;
            case "cuadrado":
                superficie = figura.getBase()*figura.getBase();
                break;
            case "triangulo":
                superficie = figura.getBase() * figura.getAltura();
                break;
        }
        figura.setSuperficie(superficie);
        return figura;
    }
}
