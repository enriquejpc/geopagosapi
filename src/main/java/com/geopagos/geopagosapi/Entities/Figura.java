package com.geopagos.geopagosapi.Entities;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "figura")
@Getter
@Setter
@NoArgsConstructor
public class Figura {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    Long id;
    @Column(name = "tipo",nullable = false)
    String tipo;
    @Column(name = "base", nullable = true)
    Double base;
    @Column(name = "altura", nullable = true)
    Double altura;
    @Column(name = "diametro", nullable = true)
    Double diametro;
    @Column(name = "superficie", nullable = false)
    Double superficie;
}
