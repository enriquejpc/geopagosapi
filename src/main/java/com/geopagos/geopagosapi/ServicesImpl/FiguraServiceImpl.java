package com.geopagos.geopagosapi.ServicesImpl;

import com.geopagos.geopagosapi.DAO.FiguraDao;
import com.geopagos.geopagosapi.Entities.Figura;
import com.geopagos.geopagosapi.IServices.IFigura;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service("figuraserviceimpl")
public class FiguraServiceImpl implements IFigura {
    @Autowired
    @Qualifier("figuraDao")
    private FiguraDao figuraDao;

    @Override
    public Figura createFigura(Figura figura) {
        return figuraDao.save(figura);
    }

    @Override
    public List<Figura> getAllFigurasList() {
        return figuraDao.findAll();
    }

    @Override
    public Optional<Figura> getFiguraById(Long id) {
        return figuraDao.findById(id);
    }

    @Override
    public List<Figura> getFiguraByTipo(String tipo) {
        return figuraDao.findFiguraByTipo(tipo);
    }
}
