package com.geopagos.geopagosapi.DAO;

import com.geopagos.geopagosapi.Entities.Figura;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.io.Serializable;
import java.util.List;

@Repository
public interface FiguraDao extends JpaRepository<Figura, Serializable> {
    public abstract List<Figura> findFiguraByTipo(@Param("tipo") String tipo);
}
