package com.geopagos.geopagosapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GeopagosapiApplication {

    public static void main(String[] args) {
        SpringApplication.run(GeopagosapiApplication.class, args);
    }

}
